var webpack = require('webpack');

module.exports = {
  entry: {
    'site': './source/javascripts/site.js',
    'layout': './source/stylesheets/layout.sass',
    'print': './source/stylesheets/print.sass',
  },

  module: {
    rules: [
      {
        test: /source\/javascripts\/.*\.js$/,
        exclude: /node_modules|\.tmp|vendor/,
        loader: 'babel-loader',
        query: {
          presets: ['env']
        },
      },

      {
        test: /source\/stylesheets\/.*(\.scss|\.sass)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'stylesheets/[name].css'
            }
          },
          {
            loader: 'extract-loader'
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'resolve-url-loader'
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      }
    ]
  },

  resolve: {
    modules: [
      __dirname + '/node_modules',
      __dirname + '/source/javascripts',
      __dirname + '/source/stylesheets',
    ],
  },

  output: {
    path: __dirname + '/.tmp/webpack-build',
    filename: 'javascripts/[name].js',
  },

  plugins: [
  ],
};
