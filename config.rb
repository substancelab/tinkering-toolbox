# frozen_string_literal: true

# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page "/*.xml", :layout => false
page "/*.json", :layout => false
page "/*.txt", :layout => false

# Reload automatically when stuff changes
activate :livereload, :host => "localhost"

# Show breadcrumbs for a page
activate :breadcrumbs

require "middleman-markdown/lib/middleman-markdown"
activate :markdown

# Localize
activate :i18n, :mount_at_root => "en"
proxy("/index.html", "/localizable/index.en.html")

# Use Webpack for building our assets
activate :external_pipeline,
  :name => :webpack,
  :command => if build?
                "./node_modules/webpack/bin/webpack.js --bail -p"
              else
                "./node_modules/webpack/bin/webpack.js --watch -d --progress --color"
              end,
  :source => ".tmp/webpack-build",
  :latency => 1

# Make Critters a blog so we can easily add extra "articles"/documentation pages
activate :blog do |blog|
  blog.layout = "documentation"
  blog.permalink = "/{lang}/{category}/documentation/{year}-{month}-{day}-{title}.html"
  blog.sources = "{lang}/documentation/{year}-{month}-{day}-{title}.html"
end

activate :directory_indexes

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

# configure :build do
#   activate :minify_css
#   activate :minify_javascript
# end
