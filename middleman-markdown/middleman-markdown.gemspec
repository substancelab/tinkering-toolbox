# frozen_string_literal: true

$LOAD_PATH.push File.expand_path("lib", __dir__)

Gem::Specification.new do |s|
  s.name = "middleman-markdown"
  s.version = "0.0.1"
  s.platform = Gem::Platform::RUBY
  s.authors = ["Jakob Skjerning"]
  s.email = ["jakob@mentalized.net"]
  s.summary = "Renders Markdown to HTML directly in your ERB templates"

  s.files = `git ls-files`.split("\n")
  s.test_files = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables = `git ls-files -- bin/*`.split("\n").map { |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_runtime_dependency("middleman-core", [">= 4.3.4"])
end
