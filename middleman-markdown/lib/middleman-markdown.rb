# frozen_string_literal: true

require "middleman-core"

Middleman::Extensions.register :markdown do
  require_relative "./middleman-markdown/extension"
  MarkdownHelpers
end
