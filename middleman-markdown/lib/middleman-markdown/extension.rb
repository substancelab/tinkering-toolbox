# frozen_string_literal: true

# Require core library
require "middleman-core"

# Extension namespace
class MarkdownHelpers < ::Middleman::Extension
  helpers do
    def markdown(&block)
      text = capture_html(&block)
      html = Tilt["markdown"].new(:context => @app) { text }.render
      concat_content(html)
    end
  end
end
