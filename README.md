# Tinkering Toolbox Educators Guide

## Site structure

* /
  * en/
    * creatures/
      * model/
        * 1/
        * 2/
      * gallery/
    * story-machines/
      * model/
        * 1/
        * 2/
      * gallery/
    * about/
    * print/
