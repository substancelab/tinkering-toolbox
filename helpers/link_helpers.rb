# frozen_string_literal: true

module LinkHelpers
  def local_path(path, options = {})
    lang = options[:language] || I18n.locale.to_s
    "/#{File.join(lang, path)}"
  end

  def our_breadcrumbs
    breadcrumbs(current_page, :separator => " > ")
  end
end
