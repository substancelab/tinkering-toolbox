# frozen_string_literal: true

module ImageHelpers
  def cloudinary_image_base_url
    "https://res.cloudinary.com/tinkeringtoolbox/image/upload"
  end

  def cloudinary_video_base_url
    "https://res.cloudinary.com/tinkeringtoolbox/video/upload"
  end

  def cloudinary_url(src, extra_transformations = [], width = "auto")
    base_transformations = [
      "c_fill",
      "g_auto",
      "w_#{width}",
    ]
    transformations = (base_transformations + extra_transformations).uniq

    [cloudinary_image_base_url, transformations.join(","), src].join("/")
  end

  def cloudinary_video_url(src)
    base_transformations = []
    transformations = (base_transformations + extra_transformations).uniq

    [cloudinary_video_base_url, transformations.join(","), src].join("/")
  end

  def responsive_image_tag(srcset_items, html_options = {})
    current_class = html_options[:class]
    new_class = [current_class, "lazyload"].compact.join(" ")

    html_options = html_options.merge(
      :class => new_class,
      :loading => "lazy",
      :data => {
        :sizes => "auto",
        :srcset => srcset_items
      }
    )

    tag(:img, html_options)
  end

  def srcset_item(url, width = "auto")
    [url, "#{width}w"].join(" ")
  end

  def srcset_sizes
    [300, 600, 900]
  end

  def styled_image_tag(src, html_options = {})
    srcset_items = srcset_sizes.flat_map do |width|
      srcset_item(
        cloudinary_url(src, styled_image_transformations, width),
        width
      )
    end

    srcset_items = srcset_items.join(",")

    responsive_image_tag(srcset_items, html_options)
  end

  def styled_image_transformations
    # photos should be edited to have a bright, warm and slightly desaturated
    # look to try to give the photos some kind of direction.
    mood_transformations = [
      "ar_16:9",
      "co_rgb:d67923",
      "e_brightness:12",
      "e_colorize:7",
      "e_saturation:-33",
    ]
  end

  def video_formats
    {
      "video/webm" => ".webm",
      "video/mp4" => ".mp4",
      "video/ogg" => ".ogv",
    }
  end

  def video_preview_tag(src, html_options = {})
    preview_transformations = [
      "ac_none", # Remove audio
      "ar_16:9", # Maintain a 16:9 aspect ratio
      "c_fill", # Fill the full area
      "e_brightness:12",
      "e_saturation:-33",
      "vs_20", # Grab 20 frames
      "w_400", # To max 400 pixels wide
    ]
    url = [cloudinary_video_base_url, preview_transformations, src + ".gif"].join("/")
    responsive_image_tag(url, html_options)
  end

  def video_transformations
    [
      "ac_none", # Remove audio
      "ar_16:9", # Maintain a 16:9 aspect ratio
      "c_fill", # Fill the full area
      "e_brightness:12",
      "e_saturation:-33",
      "w_1200", # To max 1200 pixels wide
    ]
  end

  def video_tag(src, html_options = {})
    return nil if src.nil?

    transformations = video_transformations.join(",")

    current_class = html_options[:class]
    new_class = [current_class, "lazyload"].compact.join(" ")

    poster_src = [cloudinary_video_base_url, video_transformations, src + ".jpg"].join("/")
    video_sources = video_formats.map { |mime, ext|
      video_src = [cloudinary_video_base_url, video_transformations, src + ext].join("/")
      tag(:source, :src => video_src, :type => mime)
    }

    html_options = html_options.merge(
        :class => new_class,
        :poster => poster_src
      )

    content_tag(
      :video,
      video_sources,
      html_options
    )
  end
end
