---
category: "creatures"
title: "The tale of the tail"
video: "v1562005661/creatures/IMG_3578_sfsjdd"
---
![](https://res.cloudinary.com/tinkeringtoolbox/image/upload/v1562050044/creatures/Screenshot_2019-07-02_08.46.10_asgi0a.png)
![](https://res.cloudinary.com/tinkeringtoolbox/image/upload/v1562050045/creatures/Screenshot_2019-07-02_08.46.25_ez98e2.png)

## What happened

The first critter Jeff and Andrew made was an alligator. Then Jeff asked “Shall we turn it into a dragon? Is there anything we can use for spikes for the tail?” “Andrew finds some white wheel connector blocks that look like spikes. Later, Jeff breaks the tail by mistake. Andrew “You ripped off the tail!” Jeff: “It goes faster (without the heavy tail)” The critter evolves back into an alligator - ‘Daniel the alligator’.

### Tinkering Indicators

* Using a material’s unique properties to achieve goals
* Trying ideas that might not work
* Playfully exploring [Embracing pretence]
* Connecting projects to personal interests

### Characteristics of playful experiences

* Iterative
* Joyful
* Actively engaging
* Meaningful.

## “As soon as we change something we create another problem”

![](https://res.cloudinary.com/tinkeringtoolbox/image/upload/v1562050044/creatures/Screenshot_2019-07-02_08.46.37_xorjtc.png)
![](https://res.cloudinary.com/tinkeringtoolbox/image/upload/v1562050044/creatures/Screenshot_2019-07-02_08.46.55_pbbeuj.png)

### What happened

Jeff modifies the critter. Jeff: It goes quicker but it turns. What is making it turn? Andrew: “It’s going in circles, so we are trying new legs.” Jeff changes the original small legs/feet to some larger legs/feet that he has taken from another critter he found (He later explains to a teacher “It’s not stealing it’s inspiration”). He then needs to remove the critter’s wings to accommodate the larger leg/feet. Jeff “The heads unbalanced!” Andrew “Yes, the wings gave it balance.” Jeff: “We need to make the head smaller.” (The head also hits the larger wheels) Andrew “But that’s the best part”. Jeff downsizes the head. Andrew “OK that looks nice.” But now the critter tips backwards. Andrew “As soon as we change something we create another problem!” Jeff adds a long vertical stabiliser to stop it falling backwards. Andrew: “It works perfectly! The stabiliser makes it go fast.”

### Tinkering Indicators

* Troubleshooting through iterations
* Developing workarounds
* Trying ideas that might not work
* Building on and remixing the projects of others
* Observing variables

### Characteristics of playful experiences

* Iterative
* Social
* Actively engaging
