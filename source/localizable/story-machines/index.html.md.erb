---
style: story-machines
title: Story Machines Activity
---
<% content_for :breadcrumbs do %>
  <div class="breadcrumb">
    <%= our_breadcrumbs %>
  </div>
<% end %>

<header class="full-bleed lede">
  <%= partial "introduction.md" %>
</header>

<div class="gallery">
  <%= partial "most_recent_documentation.en" %>
</div>

<section>
<% markdown do %>
This guide is intended for educators. The first part invites the reader to try the activity as a learner and get a sense of the possibilities, the second part provides more information on how to facilitate the activity for a group.

## Materials

### LEGO

#### For the base

<figure class="gallery-figure">
  <%= styled_image_tag("v1560774800/story-machines/32090195857_800e6c49f3_o_nuv8ey.jpg") %>
  <%= styled_image_tag("v1560774808/story-machines/32090196077_c67fbe2b92_o_kcxz78.jpg") %>
  <%= styled_image_tag("v1560774801/story-machines/33156626218_80654e83a9_o_gv3kgj.jpg") %>

  <figcaption class="gallery-figure-caption">
    <% markdown do %>
- LEGO Baseplate 16x32
- Thin plywood board to attach to the LEGO plate (we used a 1⁄8” thick sheet)
- 4 of LEGO brick 2x6 (this is for raising the motor)
- 3 of LEGO brick 2x4 (this is for raising the motor)
- LEGO axle 6
- LEGO bushing
- LEGO Beam 2x4 Bent 90 degrees, 2 and 4 holes
- LEGO Cross Block Beam Bent 90 degrees with 4 pins
- 3 of LEGO Technic Plate 2x4 with holes
- LEGO Beams 13
- LEGO pull-string motor
    <% end %>
  </figcaption>
</figure>

#### For building

<figure class="gallery-figure">
  <%= styled_image_tag("v1560774799/story-machines/47031800351_b1d5eac91e_o_rhesx5.jpg") %>

  <figcaption class="gallery-figure-caption">
    <% markdown do %>
Various length of LEGO Beams (we used 16, 11, 9, 7, 5)
Various length of LEGO axles (we used 4, 8, 10, 16)
Various length of LEGO Technic Brick with holes (we used 1x4, 1x8, 1x16)
LEGO Pin without friction ridges
LEGO Long Pin with Slots and No Friction
LEGO Bushing, Half Bushing
LEGO Axle to Pin Connector
LEGO Cross Block Beam Bent 90 degrees with 4 pins
Pin/half pin connector
    <% end %>
  </figcaption>
</figure>

#### Additional materials

<figure class="gallery-figure">
  <%= styled_image_tag("v1560774801/story-machines/32090196007_693e629266_o_nens1s.jpg") %>

  <figcaption class="gallery-figure-caption">
    <% markdown do %>
We’ve found adding additional materials like these helps to support the narrative component of creating a Story Machine sculpture. Learners are able to personalize their creation by drawing or collaging additional elements. Some learners even use these materials to provide additional mechanical pieces as well! Paper is an especially effective material because it is lightweight and can sit between beam-and-pin constructions and move flexibly.

- Construction paper
- Feathers
- Googly eyes
- Brads
- Pencils/Markers
- Hole punch (MUJI)
- Double sided tape
- A pair of Scissors
    <% end %>
  </figcaption>
</figure>

## Getting Started

### Build the pull-string motor base & linkage support

<figure class="gallery-figure">
  <%= styled_image_tag("v1560774797/story-machines/32090196357_1fc81d8ef5_o_kvcnn3.jpg") %>
  <%= styled_image_tag("v1560774797/story-machines/33156625848_1c5f6571b2_o_ent02r.jpg") %>

  <figcaption class="gallery-figure-caption">
    <% markdown do %>
To get started, use double stick tape to attach your base plate to a thin sheet of plywood for additional support. This will allow you to move or lift your sculpture without it bending or breaking.
    <% end %>
  </figcaption>
</figure>

<figure class="gallery-figure">
  <%= styled_image_tag("v1560774797/story-machines/32090196207_30b4797c6a_o_nsz3if.jpg") %>
  <%= styled_image_tag("v1560774793/story-machines/32090196627_f82fe97a2b_o_gpmbyl.jpg") %>
  <%= styled_image_tag("v1565687127/story-machines/47181062501_c457005343_o_oqn8pv.jpg") %>

  <figcaption class="gallery-figure-caption">
    Next, build a base for your pull-string motor to raise its height. We like using an alternating pattern of two-2x6 bricks, then three-2x4 bricks, then repeating two-2x6 bricks. This pattern provides stability when pulling the string. You may also choose to super glue these bricks to the motor, and super glue the whole assembly to the base (with the pull-string going off the edge). The resistance of the spring inside the motor is powerful and can pull your creation apart if you’re not careful. If you don’t want to glue the pieces together, be sure to remember to hold the motor in place with your hand every time you pull the string.
  </figcaption>
</figure>

<figure class="gallery-figure">
  <%= styled_image_tag("v1560774800/story-machines/32090195857_800e6c49f3_o_nuv8ey.jpg") %>

  <figcaption class="gallery-figure-caption">
    <% markdown do %>

For the crank, use an axle 6 through the motor with a LEGO Beam 2x4 Bent 90 degrees (2 and 4 holes) on one side and a bushing on the other to hold it in place. The L-shaped beam should spin smoothly when you activate the motor, but not wiggle back and forth too much.

For making your linkage, it’s helpful to have an additional vertical support. We like to use three-2x4 LEGO technic plates with holes stacked on top of each other attached to a LEGO Cross Block Beam Bent 90 degrees with 4 pins. Attach a 16-hole beam to the remaining two pins. You may choose to glue this assembly together as well, but ​don’t​ glue it to the base plate. You’ll want to be able to move it later as you build your linkage.
    <% end %>
  </figcaption>
</figure>

### Tinker with making a linkage

<figure class="gallery-figure">
  <%= styled_image_tag("v1565689527/story-machines/33156626328_b363faca3d_o_skjrv4.jpg") %>
  <%= styled_image_tag("v1560774799/story-machines/40216269823_cf081f8e59_o_lpi1yh.jpg") %>

  <figcaption class="gallery-figure-caption">
    <% markdown do %>

Linkages can take many shapes and forms. For building your Story Machine, start by attaching two 16-hole beams at one end with a gray connector pin. Use the gray pins to attach a one beam to your L-shaped crank and the other to your vertical support. As you put these pieces together, have a look from above to make sure things are lined up in even planes and not pulling backwards or forwards.

Activate the motor (don’t forget to hold it when you pull the string!) and notice what happens. How are the beams moving? Can they move freely, or is there a point where they get stuck? This is the point in the process where you can really start experimenting! Notice what changes when you reconfigure the pieces. Some things you can try are:

- Changing where your beam is attached to the L-shaped crank
- Changing where the two beams meet (end-to-end, T-intersection, or crossed)
- Changing where the second beam attaches to the vertical support
- Changing the distance of the vertical support from the motor (making it -loser or farther away)
- Experimenting with the length of the beams
    <% end %>
  </figcaption>
</figure>

<figure class="gallery-figure gallery">
  <%= video_tag("v1560774790/story-machines/32239290657_66855341f9_o_qvmdzx", :class => "gallery-item gallery-video", :controls => true) %>
  <%= video_tag("v1565693602/story-machines/47181074671_rl5dea", :class => "gallery-item gallery-video", :controls => true) %>
  <%= video_tag("v1565693784/story-machines/47181073461_gbauo1", :class => "gallery-item gallery-video", :controls => true) %>

  <figcaption class="gallery-figure-caption">
    <% markdown do %>
Most importantly, remember that linkages are tricky! They take time and iteration to troubleshoot, and often will move in ways you don’t expect. Over time, you’ll get the hang of figuring out how to make them move the way you want them to.

LEGO is an especially useful material for tinkering with linkages because it allows for iteration in the design process. As you build you can track what different positions and configurations you’ve tried, and it’s easy to go back and undo those changes if you’d like to revisit an earlier idea. While that’s still a possibility with materials like cardboard and wire, undoing a change can be much more challenging.

Here are a few starting points: you can make linkages that move up and down, swing back and forth, or open and close like a pair of scissors. When facilitating this activity for a group of learners, we like to have many possible starting point linkages already built, and encourage learners to start experimenting by making changes to the existing structures.
    <% end %>
  </figcaption>
</figure>

## Experiment and explore the mechanism

<div class="gallery">
  <%= video_tag("v1560774786/story-machines/47129223532_c92aa53fe2_o_ybruje", :class => "gallery-item gallery-video", :controls => true) %>
  <%= video_tag("v1560774780/story-machines/40216275143_6b49c544ee_o_obkfpy", :class => "gallery-item gallery-video", :controls => true) %>
  <%= video_tag("v1560774784/story-machines/47129222442_30f3b0bd8c_o_lpxjiu", :class => "gallery-item gallery-video", :controls => true) %>
</div>

As you gain comfort with how your linkage moves, you can add complexity by building on secondary movements or adding angled or extension pieces to exaggerate the motion.

## Design your animated sculpture

<div class="gallery">
  <%= video_tag("v1560774784/story-machines/46296134885_b201117169_o_mxty7p", :class => "gallery-item gallery-video", :controls => true) %>
  <%= video_tag("v1560774781/story-machines/47000419411_0075f97cf4_o_ld2l2x", :class => "gallery-item gallery-video", :controls => true) %>
</div>

As you’re building, you may ask yourself, “What does this motion remind me of?” Could it be a person waving an arm? A boat on the sea? The snapping jaws of an alligator? The possibilities are endless! Alternately, you can consider what story you want to tell and how you can use the linkages in your Story Machine to add motion to that story. When working with construction paper, you can create individual elements to add onto your linkage or create more complex jointed pieces. Double sided tape is helpful for attaching pieces to beams or onto the base plate. Brads and small hole punches can also add jointed motion. You may also choose to add feathers, googly eyes, or other craft materials to give your creation personality.

## Take it Further

<%= styled_image_tag("v1560774772/story-machines/32239276127_572713c2a4_o_fxp6lx.jpg") %>
<%= styled_image_tag("v1560774771/story-machines/40216270893_58b9c8b8d9_o_uwambt.jpg") %>

Automata and linkages can be made out of many different materials and in many different sizes. You can explore using cardboard, foam, trash, wire, and wood for making the core of your story-machines design. You can also play around with scale to see how big or small you can make them. You can even try alternating between 2D and 3D designs for what you create. (IMAGES - cranky contraption with linkages, trash story-machines, cardboard linkage)

There are many artists who explore automata as their primary medium. We get inspiration from those artists’ works and find that we can learn building techniques by observing their creations. Some automata artists who have influenced us are:

- Keith Newstead
- Noga Elhassid
- Kazu Harada
- Hernán Lira

## Facilitation Techniques

<section class="step-by-step">
  <div class="step-by-step-gallery">
    <%= styled_image_tag("v1565695906/story-machines/31446592187_321bbc55bf_o_icykws.jpg") %>
    <%= styled_image_tag("v1565696161/story-machines/45473388205_0a1639464e_o_u1kuoe.jpg") %>
  </div>
  <div class="step-by-step-text">
    <% markdown do %>
### Try it first as a learner

Taking time to try out the activity as a learner is vital before facilitating it for others. This process allows you to have firsthand experience with the tools and materials for building. You’ll discover what works well and where potential sticking points may arise. You’ll learn how to troubleshoot as you build, and can have a better sense of how to guide learners through their own design process. Trying activities as a learner also builds empathy - you can recall what it feels like to experience building for the first time. Just like you, learners may not know what to do or try next and as a facilitator you can be more equipped to support them.

### Set up a rich environment for tinkering

We’ve found it helpful to consider what the environment for tinkering feels like. Some points to think about are: What does the introduction to the workshop space feel like? Where are examples displayed? How are materials arranged and made accessible? For Story Machine, we often set up an introductory table with a few examples for learners to try before coming into the workshop area to build. On the work table, we’ll put out all the LEGO materials and tools in trays or containers. We keep the crafting materials on smaller table nearby because they’re important to the activity but often aren’t needed until later in the process. Encouraging participants to get up and move through the space also helps to cross-pollinate ideas when learners can see what others have tried.

### Welcome learners into the activity

We try to frame this activity in a way that welcomes the learners’ ideas and is grounded in exploration of the materials and phenomena. Often we’ll start by saying something like, “Today we’re building Story Machine, which are animated kinetic sculptures. They are activated by a pull-string motor, and have a LEGO ‘skeleton’ in the back that drives the motion to make your character or story come to life.” Once learners have a sense of what the activity is about, we’ll introduce the materials and provide a space to get started building. Which leads to...

### Have many starting points available for use

We provide base plate models with several different linkage configurations for learners to get started building. It’s important to have many types of linkage starting points available to show the variety of potential outcomes. Different motions may also inspire different ideas for building.

### Encourage iteration on linkage design from initial starting point

Once learners have a starting point linkage to work from, it’s helpful to encourage them to experiment with different configurations to see how that changes the movement. Point out a few places that can be moved around, such as where the beams connect or where the second beam attaches to the vertical support. You can also suggest that they can add or remove pieces to see how that changes things.
    <% end %>
  </div>
</section>

<section class="step-by-step step-by-step-text-first">
  <div class="step-by-step-gallery">
    <%= styled_image_tag("v1560774768/story-machines/47000419191_2df7b0f487_o_xrcdbf.jpg") %>
  </div>
  <div class="step-by-step-text">
    <% markdown do %>
### Encourage open-ended outcomes

Learners will all have different ideas for what they want their Automata to depict. Craft materials have strong expressive qualities that will allow learners to personalize their creations. Story Machine are individual to their creators, so each one will be different. It’s not a competition to make the best one; it’s about each learners’ journey through the process of tinkering to achieve their personal goal.

### And don’t forget: Hold the motor when you pull the string!

If the motor isn’t firmly attached to the base, it can cause your sculpture to come apart when you pull the string. This is disappointing, because then you have to fix the creation you’re working on (which is not the good frustration that can come from tinkering). It can also create an unsafe situation where LEGOs can fly in unexpected directions.

### Ask questions to discover the learners’ intentions and ideas

Open-ended questions are an excellent technique for drawing out learners’ goals, ideas, prior history with the phenomena, and current understanding of what they’re working on. For this activity, it’s important as a facilitator to learn both what kind of movement they want to design and how that movement will connect with their narrative elements. Some things you might ask are:

- “What are you working on right now?”
- “Can you show me what it’s doing?”
- “What does this motion remind you of?”
- “Which parts of your sculpture do you want to move?”
- “What’s something we could try to make your idea work?”

Most importantly, use these questions to draw out learners’ ideas, not quiz them for “correct” answers.
    <% end %>
  </div>
</section>
<% end %>
