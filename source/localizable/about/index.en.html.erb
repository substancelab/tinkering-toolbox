---
style: about
title: LEGOTinkering
---
<header class="lede">
  <div class="lede-header">
    <div class="breadcrumb">
      <%= our_breadcrumbs %>
    </div>
    <h1 class="lede-header-title">LEGOTinkering</h1>
  </div>
</header>

<section>
<% markdown do %>
## Introduction and context

LEGO Tinkering is an approach to thinking and learning that emphasizes the importance for learners of all ages to think with their hands by exploring materials, building objects, constructing knowledge, and developing personally meaningful understanding. This can be accomplished through a set of carefully organized LEGOTinkering project starters (the LEGOTinkering Exploration Kit) offering a variety of ways for educators to practice their facilitation strategies and observe the various outcomes that each project may take.

Ultimately, these experiences support a learner’s unique pathway of understanding, builds a positive relationship between the teacher and student, and develops a repertoire of cumulative experiences that becomes a deeper understanding over time.

<%= styled_image_tag("v1560777773/creatures/45499494671_49452421d9_o_q1pagq.jpg") %>

The LEGOTinkering exploration kit has two goals. The first is to create a meaningful open-ended play experience for children. These explorations can be both easy to understand and easy to facilitate for educators new to open-ended creativity. Meaningful explorations can spark curiosity for children, and allow them to pursue their own pathway of exploration as a way of creating new and interesting projects (many that we haven’t even imagined yet).

The second goal is more experimental, and has to do with the educators who are using the box. We invite you all to pay close attention to the children and reflect on what is happening as they are learning through play with the activities. The project starting points are intended for you to gain some experience building and tinkering yourself, and then practice your facilitation of these experiences as you invite other learners to play along.


## What is Tinkering

Tinkering is an age-old practice of mending and repairing household appliances, tools, and toys. The tinker would need to deal with each situation using what was at hand and whatever knowledge they already had, through a process that combined tried and true methods with improvisation and experimentation. In this practice new problems are seen as opportunities to build knowledge, and new possibilities for learning and refining their craft emerge naturally.

<%= styled_image_tag("v1560778382/creatures/46880483662_13ec103f5c_k_xbqxgf.jpg") %>

As an educational practice and stance, tinkering for us means valuing the learner’s existing knowledge, interests, and competence. It means valuing posing new problems more than coming up with clever solutions, and trusting that the process of exploration, experimentation, and expression that is at the core of tinkering is a worthwhile pursuit in and of itself.

Children are natural tinkerers, and this process is fundamental to the way they develop understanding of the world and the phenomena around them. Children are naturally suited to explore their environment through endless curiosity, to notice phenomena — both prominent and subtle — through their sharp powers of observation, and to enter into a conversation with materials and processes through making and manipulating. For children, understanding often goes through their hands and bodies first. They investigate through playful invention and self-expression, and in this process gain experience coming up with ideas, creating projects with a variety of media, carrying out new projects, collaborating with others, and reflecting on (and revising) their ideas.

When supported in their tinkering process, children develop a disposition that is at the heart of this approach: that the world is knowable, and they have agency in how they build the knowledge to navigate through it. Tinkering experiences build creative abilities, support new inquiry-based capacities, and engage learners in projects that are joyful, aesthetically-rich, personally meaningful, and iterative over time. LEGO Tinkering was developed to help educators and designers understand how a tinkering approach can support children’s learning.

<section class="step-by-step">
  <div class="step-by-step-gallery">
    <%= styled_image_tag("v1565696161/story-machines/45473388205_0a1639464e_o_u1kuoe.jpg") %>
    <%= styled_image_tag("v1560778378/creatures/45436890231_e7f1360267_k_eokgyb.jpg") %>
  </div>
  <div class="step-by-step-text">
    <% markdown do %>
### Designing for tinkering

A tinkering experience is by definition unique to the specific learner who engages in it, so in designing for tinkering we think of our role as setting the context in which learners can safely and creatively explore a tinkering process. Thinking as educators, our role is not to set an end point or specific content that we want everyone to achieve, but rather to “tilt the field” in the direction of a rich exploration of a phenomenon. Our approach as facilitators in this process is to be attentive to the goals and processes each learner is engaging with, and gently guide them as needed, being less concerned that each learner ends up with a neat final product, and more focused on the ideas that emerge in the process, the problems that are posed, and the solutions that are attempted and what they reveal about their developing understanding. Broadly speaking we pay attention to three aspects of the tinkering experience.

### Environment

We try to create an environment to tinker in that is welcoming and a comfortable space to “not know” something. Tinkering often pushes us to the edge of our current understanding and urges us to go a little bit further, to try the unknown; this can be a frustrating process and puts learners in a vulnerable position, so we want the environment to communicate a sense of safety and comfort. Natural materials, good lighting, comfortable seating arrangements, evidence of past work and tentative ideas, and a good dose of humor and whimsy are all tools that can be used to achieve this.
    <% end %>
  </div>
</section>

<section class="step-by-step step-by-step-text-first">
  <div class="step-by-step-gallery">
    <%= styled_image_tag("v1560778379/creatures/44501595875_7baf459316_k_ipzoqo.jpg") %>
    <%= styled_image_tag("v1565695906/story-machines/31446592187_321bbc55bf_o_icykws.jpg") %>
  </div>
  <div class="step-by-step-text">
    <% markdown do %>
### Activity design

Tinkering is not an open invitation to make “whatever you want” with no educational goals; rather, we put extraordinary effort into designing activities with intentionality and purpose, that set up a clearly defined problem space, within which learners have a large latitude to explore, pose new problems, and experiment with solutions. We put an explicit emphasis on collaboration rather than competition, and tend to stay away from framing the experience as a challenge. A mark of a well-designed activity for us is when it conforms to Mitchel Resnick’s metaphor of having: a l​ow threshold,​meaning there are accessible ways to get started and reach early successes; a ​high ceiling,​meaning the activity design supports deepening the experience to explore increasingly complex concepts, techniques, and solutions; and ​wide walls,​meaning there are opportunities to expand beyond the initial scope of the activity, explore tangential, offbeat, one-off ideas that we had never seen before.

### Facilitation

Facilitation is the not-so-secret ingredient that is crucial to a rich tinkering experience. We see facilitation as a practice more akin to an art than a science, requiring the facilitator to think of their role as a co-learner rather than a teacher. This means keeping a curious and open attitude toward the ideas of the learner, which can be tentative and sometimes, to an experienced maker, not very productive or effective. We try to keep in mind that t​he big idea is their idea,​ and our facilitation is in service of sparking, sustaining, and deepening their explorations, and not evaluating their solutions. This guide will include specific moments in each of the activity starters where you (as a facilitator) can step into the learner’s investigations and ​Spark, Sustain​, or ​Deepen ​their investigations.
    <% end %>
  </div>
</section>


<% end %>
</section>
