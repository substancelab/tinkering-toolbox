---
style: creatures
title: Creatures Activity
---
<% content_for :breadcrumbs do %>
  <div class="breadcrumb">
    <%= our_breadcrumbs %>
  </div>
<% end %>

<header class="full-bleed lede">
  <%= partial "introduction.md" %>
</header>

<div class="gallery">
  <%= partial "most_recent_documentation.en" %>
</div>

<section>
<% markdown do %>
Photos: ​Child building at environment table​, ​Child adding rock to critter​, ​Critter in its environment

This guide is intended for educators. The first part invites the reader to try the activity as a learner and get a sense of the possibilities, the second part provides more information on how to facilitate the activity for a group.

## Materials

### LEGO

- Various length of LEGO Beams (we used 9, 7, 5)
- 1 x 2 beam with pin hole and axle hole
- 3 x 0.5 beam with pin hole and two axle holes
- 3 x 3 T-shaped
- Bent beams of various lengths
- Various length of LEGO axles (we used 4, 8, 10)
- Axle 2 with grooves
- Various length of LEGO Technic Brick with holes (we used 1x4, 1x8)
- 1x2 brick with axle hole
- 1x2 brick with pin hole
- Various size of LEGO gears (we used 24 teeth)
- Angle connector
- LEGO Pin without friction ridges
- LEGO Long Pin with Slots and No Friction
- LEGO Bushing, Half Bushing
- LEGO Axle to Pin Connector
- Pin/half pin connector
- Various LEGO eyes and faces

### Additional Materials

We’ve found that adding non-LEGO materials supports variety in critter characters and their unique motions. Wings, antennae, and silly expressions are all possibilities with paper. Paper is an especially effective material because it is lightweight and can sit between beam-and-pin constructions and move flexibly.

Paper crafting as an entry point into a LEGO activity also supports learners who are newer or less-fluent in LEGO materials. Seeing a familiar material used in an unfamiliar way can bridge the divide into using the new material. We’ve seen learners who have little to no experience using LEGO Technic pieces create elaborate and creative creatures by channeling their paper crafting skills and experimenting with LEGO pieces.

- Paper
- Double stick tape
- Muji LEGO paper hole punch (optional)
- Scissors
- Pencils and markers

We also use cardboard and wooden cams with axle holes in a variety of irregular shapes. These motions are often jerky and halting and add extra personality to creatures. Assembled asymmetrically a critter can truly have a unique movement.

- Laser cut cams and irregularly shaped wheels

## Critter habitat

Rich critter habitats are crucial for this activity. A lush landscape of textures changes the ways creatures move and sparks imagination and idea generation for fantastic creatures and characters. Rough surfaces like sand paper provide friction to propel a critter forward, while a slippery sheet of plastic may afford only a small glide. Critter designs can be greatly impacted by the surfaces they encounter, so a variety of types is essential.

(Photo: ​https://flic.kr/p/2aNsj3p​, ​https://flic.kr/p/2eqEK1d​, ​https://flic.kr/p/23TS94H​, ​https://flic.kr/p/NKcEzp​)

- Aluminum foil
- Sand paper
- Corrugated cardboard
- Faux fur
- Rocks and stones
- Bubble wrap
- Other natural materials, e.g, small pieces of wood, leaves
<% end %>
</section>

<section class="step-by-step">
  <div class="step-by-step-gallery">
    <%= styled_image_tag("v1560777773/creatures/45499492091_c453d3aeb5_o_njcbqf.jpg") %>
    <%= styled_image_tag("v1560777773/creatures/45499494671_49452421d9_o_q1pagq.jpg") %>
    <%= styled_image_tag("v1560777772/creatures/31626836528_2a79452e1c_o_xtkx6w.jpg") %>
  </div>
  <div class="step-by-step-text">
    <% markdown do %>
## Getting Started

### Build a Base Model (​https://flic.kr/p/SMV22b​)

To get started, build one of the three base model options below. These starting points can also be given to learners when they are first introduced to the activity as places to build from. A base model provides enough information to begin creating a critter while also not finished enough to feel complete.

One activity design consideration we made was to avoid materials that are wheel-like that can be made into a car. This is not what we would consider to be a critter. Rough, jerky motions create more personality and are more likely to lead to unexpected discoveries than a smooth, even movement. That being said, we’ve found that gears make for interesting wheels because they are more robust and can traverse rough surfaces.

### Option 1: ​The arm​ ​(and linkage) ​(​LEGO beam 1 x 2 with axle hole and pin hole​)

This LEGO beam provides a strong connection for the cross-shaped axle as well as a pin hole. A pin can affix this beam to another, longer beam and allow the two pieces to rotate relative to one another. This loose connection facilitates building linkages from the axle to build onto (see Option 4). The two pin length is also too short to propel the motor forward, requiring the learner to build onto the beam in some fashion.

The 1x2 beam can be given as is, but we often find that this is not substantial enough of a starting place. We add a simple linkage to the beam using a pin connector as a base model to build from. A linkage creates an interesting mechanical motion that can be easily modified.

### Option 2: T​he cam

Photo: ​Critter with wooden cams

A non-circular shaped piece cut out of a rigid material. We laser cut wood with an axle hole to attach to the motor and tried a variety of shapes. We like seeing asymmetrical cams added to creatures because of the way creatures wiggle and wobble as they propel themselves forward.

### Option 3: The brick (​horizontal​ and ​vertical​) (​LEGO technic 1 x 2 brick with axle hole​)

Similar to the arm, the technic brick is a great building-off point for many different ideas and is too short to propel the critter forward. The two studs on top make easy brick-to-brick connections and other types of fixed (or non-rotating) connections.

The orientation of the motor can inspire different pathways for exploration. Similar to the previous options, the motor can be placed horizontally. It can also be placed vertically with the spinning disk touching the tabletop. In this orientation, the motion of the critter changes and harnesses the disk as well as the axle to propel it forward.
    <% end %>
  </div>
</section>

<section>
<% markdown do %>
## Tinker with motion and movement

After building a base model, pull the string and notice its motion. You will find that the motion is not wholly satisfactory. Consider different extensions that can be added to these base pieces. Add another piece and notice the new motion. Explore adding various LEGO pieces and observe how they impact the critter’s movement. You will find that not all additions prove fruitful, and frequent iteration is necessary to produce interesting motions.

## Test in the environment

Photos: ​Child testing critter design​, ​Testing multiple designs in an environment

Before making too much progress on the critter, it’s important to bring it over to the testing environment. This environment can be built before any critter creation or as a result of critter explorations. The ways in which the critter interacts with its environment can inform modifications and enhancements to its design and inspire more specific design goals (i.e. walking, climbing a slope, etc.).

## Add decorations

Mixing LEGO with non-LEGO materials is an important part of this activity. The contrast in materials create friendly and personalizable creatures. Similar to the [Story Machines activity](<%= local_path("/story-machines/") %>), paper cut outs are attached with hole punches and double stick tape. Adding eyes is an important detail for a critter; we see many learners give their creatures a face, making it that much more realistic.

Decoration leads into storytelling and narrative development around a critter. The narrative emerges from the experience of creating the critter; learners generally do not know what type of critter they want to create ahead of time. The critter personality is a product of trial and error with its design.

## Habitat explorations

### Multi-textured surfaces

Photos: ​Environment table from above Critter on cardboard topography Testing a critter in environment

We built a compelling tabletop testing environment for creatures to roam around in. These surfaces had a variety of textures and changes in elevation, resulting in a rich environment for exploration. Creatures wiggle their way across the different materials and learners will find that their critter works better on some surfaces than others. This understanding informs modifications and design considerations as learners continue to build and iterate on their idea.

The textured testing area is an open-ended exploration space and is separate from the building station. We set up a build zone where LEGO pieces and building materials are within reach. We find that having these two distinct areas helps learners to be intentional about which area they are in. When they feel that they have a complete critter, they are able to go and test it in an area dedicated to wandering creatures. And if they feel changes need to be made, they are able to access the complete library of materials back at the starting station. At both stations, learners can easily see what others are doing and have access to inspiring projects from their co-learners. Seeing others’ work encourages collaboration and we value when participants build on the ideas of others.

Educators should be aware that the motors are too weak for creatures to traverse very textured materials or steep slopes. Though we exclusively use lightweight materials, the motors were not strong enough to dig into the ground and propel them forward. We found that a stronger motor leads to more variety in learners’ explorations and were successful using commercially available LEGO wind-up motors in this case (​https://www.bricksandfigs.com/products/lego-parts-wind-up-motor-2-x-6-x-2-1-3-with-raised-shaft-base-long-axle- dark-gray​) .

### Obstacles

Photo: ​Critter inside a tunnel

Tunnels and sloping mountains are interesting challenges for learners to encounter with their creatures. We made a mountain like a topographic map with gentle, cardboard steps that were easy to climb. Tunnels are an interesting addition to see if creatures walk in a straight line and if they can make it to the other side.

It’s important to clarify that this is not a race track or an obstacle course. The purpose of adding obstacles is not for competition, but instead to encourage learners to make purposeful design choices as they build their critter. It may do very well in one area of the table and less so in another, and we value when learners set their own goals for their creatures.

### Switch it up

We like to encourage learners to design and modify environments for critter exploration. The process of switching back and forth between creating creatures and building environments allows for opportunities to build confidence with a range of materials, express intentionality in their designs, and applying unique solutions to their specific environment.

## Take it further

Run over a couple of sessions the creatures activity can yield complex and diverse results. When given sufficient time, a learner can continue to build on their ideas and complexify their critter. Encouraging open ended explorations like this can result in a variety of outcomes across learners.

- Butterfly
- Spider
- Critter with cams
- Wooden wheels
- Back beam
- Linkage and cork bottom
<% end %>
</section>

<section class="step-by-step step-by-step-text-first">
  <div class="step-by-step-gallery">
    <%= styled_image_tag("creatures/46880483662_13ec103f5c_k_xbqxgf.jpg") %>
    <%= styled_image_tag("creatures/45436890231_e7f1360267_k_eokgyb.jpg") %>
  </div>
  <div class="step-by-step-text">
    <% markdown do %>
## Facilitation techniques

As with any tinkering activity, we’ve found that thoughtful facilitation is crucial for supporting learners throughout their process. While some of these tips below are specific to creating creatures and their habitats, others are more general and can be applied to any tinkering activity. As you reflect on your role as a facilitator, pay attention to how your words and actions impact what learners do and try in the space.

### Try it first as a learner

Taking time to try out the activity as a learner is vital before facilitating it for others. This process allows you to have firsthand experience with the tools and materials for building. You’ll discover what works well and where potential sticking points may arise. You’ll learn how to troubleshoot as you build, and can have a better sense of how to guide learners through their own design process. Trying activities as a learner also builds empathy - you can recall what it feels like to experience building for the first time. Just like you, learners may not know what to do or try next and as a facilitator you can be more equipped to support them.

### Set up a rich environment for tinkering

We’ve found it helpful to consider what the environment for tinkering feels like. Some points to think about are: What does the introduction to the workshop space feel like? Where are examples displayed? How are materials arranged and made accessible? For creatures, we’ve found that having the LEGO pieces in divided bins on the building table made the materials accessible for creating. We then placed the testing environments on separate tables so that learners were required to move themselves from the building area to the testing area. Testing spaces were populated with example creatures and richly textured surfaces. We kept crafting materials at a small table nearby because they are important to the activity but not needed until later in the process.

### Welcome learners into the activity

We try to frame the activity so that learners understand that they are able to explore their own ideas in our space while also experimenting with motions and mechanisms. We might say, “Today we are creating creatures that move with a motor. When I pull this string, notice how the critter moves across this table. Each critter moves in a different way, and we can see how it moves in this environment.”

### Spark learners’ interest

Demonstrate for learners an example of a moving critter. Show, don’t tell. Let them play with the creatures and explore how they move across a variety of surfaces. This is an opportunity for learners to engage with a critter before investing in a design or direction. Provide a variety of examples so learners can see a range of directions for their own designs.

### Have many starting points for use

We offer a starting point for learners: a wind up motor, cross axle, and a piece (either LEGO or not) that propels the motor forward. The movement of a critter can communicate a personality and make a compelling creature. We want learners to engage with materials that can make a wide variety of quirky movements. The activity is designed for learners to explore motion and to use motion as a form of storytelling.

### Encourage iteration from initial starting point through environment tests

Once learners have a starting point to work from, it’s helpful to encourage them to experiment with different configurations to see how that changes the movement. Point out that a few places can be adjusted, as well as places where pieces can be added or removed to see how that changes the critter’s motion.

### Ask questions to discover learners’ intentions and ideas

Open-ended questions are an excellent technique for drawing out learners’ goals, ideas, prior history with the phenomena, and current understanding of what they’re working on. For this activity, it’s important as a facilitator to learn about what pieces learners have tried and how their designs respond to the environment. Some things you might ask are:

- “What are you working on right now?”
- “Can you show me what it’s doing?”
- “What type of critter would move like this?”
- “What did you notice when your critter was in the environment?”
- “What changes could you make for it to move differently?”

Most importantly, use these questions to draw out learners’ ideas, not quiz them for “correct” answers.

### Encourage open ended outcomes

The combination of LEGO and non-LEGO materials gives learners the opportunity to create unique creatures. We use the word critter intentionally to keep the possibilities open for all types of moving characters. A hallmark of a well-designed tinkering activity is one that produces a wide variety of outcomes. It’s not a competition to make the best one; it’s about each learners’ journey through the process of tinkering to achieve their personal goal.
    <% end %>
  </div>
</section>
